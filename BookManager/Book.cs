﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookManager
{
    class Book
    {
        private string _title;
        private string _author;
        private int _pages;
        private int _wordCount;

        private static int _createdBooks;

        public Book(string title, string author)
        {
            this._title = title;
            this._author = author;
            _createdBooks++;
        }

         static Book()
        {
            _createdBooks = 0;

        }

        public string GetTitle() => _title;
        public void SetTitle(string title)
        {

            if (title != null && title.Split(' ').Length > 0)
                this._title = title;

        }

        public string GetAuthor() => _author;
        public void SetAuthor(string author)
        {

            if (author != null && author.Split(' ').Length > 0)
                this._author = author;

        }
            

        public int GetPageCount() => _pages;
        public int SetPageCount(int pageCount) => _pages = pageCount;

        public int GetWordCount() => _wordCount;
        public int SetWordCount(int wordCount) => this._wordCount = wordCount;

        public int GetTitleLength() => _title.Length;

        public static int GetCreatedBooks() => _createdBooks;
        

    }
}
