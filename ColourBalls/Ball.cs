using System;

namespace ColourBalls
{
    class Ball
    {
        private float _size;
        private Colour _colour;
        private int _timesThrown;

        public Ball(float size, Colour colour)
        {
            if(size > 0) _size = size;
            else size = 1;

            _colour = colour;


        }
        
        public void Pop () => _size = 0;
        public void Throw() 
        {
            if(_size > 0) 
                _timesThrown ++;

        }

        public int GetTimesThrown() => _timesThrown;

    }
}
