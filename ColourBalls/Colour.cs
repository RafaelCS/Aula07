using System;

namespace ColourBalls
{
    class Colour
    {
            private int _red;
            private int _green;
            private int _blue;
            private int _alpha;

            //bugs no spoilers pls

            //Construtors
            public Colour(int red, int green, int blue, int alpha)
            {
                SetRed(red);
                SetGreen(green);
                SetBlue(blue);

                SetAlpha(alpha);


            }

            public Colour(int red, int green, int blue)
            {
                SetRed(red);
                SetGreen(green);
                SetBlue(blue);              
                
               _alpha = 255;
            }

            //Getters and Setters

            public int GetRed() => _red;
            public int GetGreen() => _green;
            public int GetBlue() => _blue;

            public int GetAlpha() => _alpha;

            public void SetRed(int rValue)
            {
                if(rValue > 0 && rValue < 255 ) 
                    _red = rValue;
            }

            public void SetGreen(int gValue)
            {
                if(gValue > 0 && gValue < 255 ) 
                    _green = gValue;
            }

            public void SetBlue(int bValue)
            {
                if(bValue > 0 && bValue < 255 ) 
                    _blue = bValue;
            }

            public void SetAlpha(int aValue)
            {
                if(aValue > 0 && aValue < 255 ) 
                    _alpha = aValue;
            }

            //Methods
            public float GetGreyScale()
            {

                return ( (_red + _green + _blue) / 3 );

            } 




    }
}
