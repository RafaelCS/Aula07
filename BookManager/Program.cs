﻿using System;

namespace BookManager
{
    class Program
    {
        //bibliografia da cadeira
        //https://www.dropbox.com/s/aefn936d31o5y6e/LP1.zip?dl=0

        static void Main(string[] args)
        {

            //Variáveis
            Book[] books;

            //Pedir o nº de livros ao utilizador
            Console.WriteLine("Welcome to the book fair app, " +
                "how many books will you be submitting?");

            // Assumindo que o string pode ser convertido para int sem probs
            books = new Book[Int32.Parse(Console.ReadLine())];

            //Pedir pelo title nome de autor para
            //cada livro individualmente
            for (int i = 0; i < books.Length; i++)
            {
                // Variáveis
                string tempTitle;
                string tempAuthor;

                
                Console.WriteLine("Input the Title for book nº" + (i+1) );
                tempTitle = Console.ReadLine();

                Console.WriteLine("Input the Author's name for book nº" + (i+1) );
                tempAuthor = Console.ReadLine();

                //Criar um objecto livro com os parâmetros especificados
                //e colocá-lo na posição correcto no array books[]
                books[i] = new Book(tempTitle, tempAuthor);

            }

            Console.WriteLine("\nAll done!\n" +
                "Here's your book order:");

            //Mostrar quantos carateres tem o titlo, o titlo em si
            //e o nome do autor.
            foreach (Book b in books)
            {
                Console.WriteLine($"With a title length of " +
                    $"{b.GetTitleLength()} characters:" +
                    $"\n\t'{b.GetTitle()}', by {b.GetAuthor()}");

            }

            Console.WriteLine(Book.GetCreatedBooks());
            //Impede o programam de se fechar automaticamente
            Console.ReadLine();

        }
    }
}
